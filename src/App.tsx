import React from 'react';
import {ToastContainer} from "react-toastify";
import RootLayout from './root-layout';

function App() {
    return (
        <>
            <RootLayout/>
            <ToastContainer />
        </>
    );
}

export default App;
