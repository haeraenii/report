import React from "react";
import ReportContainer from "../report-container";

const CustomerInsu = () => {
    return (
        <ReportContainer>
            <h1 className="title">보유계약 현황 남한희님</h1>
            <p className="explain">남한희님이 보유하고 있는 계약은 총 <span className="stress">2건</span>입니다.</p>
            <div className="family-insu">
                <div className="content-area">
                    <div className="content-box page2-box1">
                        <div className="left-side">
                            <p>
                                <span className="name"><img src="/img/report/family2.png" alt="고객이미지"/>남한희 고객님</span>
                                <span className="cus-info">
                                    1991.09.17 (여 31세)<br/>
                                    보험상령일 2022년 03월 17일
                                </span>
                            </p>
                            <p>
                                <span className="insu-monthly"><img src="/img/report/icon-money-darkblue.png" alt="돈이미지"/>월보험료</span>
                                <span><strong>160,072</strong>원</span>
                            </p>
                        </div>
                        <div className="right-side">
                            <div className="chart-circle">그래프</div>
                            <p><span>·기납입보험료</span><strong>484만원</strong></p>
                            <p><span>·잔여보험료</span><strong>5,503만원</strong></p>
                            <p><span>·총보험료</span><strong>5,987만원</strong></p>
                        </div>
                    </div>
                    <div className="content-box chart-box">
                        <div className="chart-desc">
                            <i><img src="/img/report/icon-nothing.png" alt="수준 이미지"/></i>
                            <p className="chart-result">
                                총 48개의 보장 중<br/>
                                <span>24개의 보장</span>에 걸친<br/>
                                점검이 필요합니다. 
                            </p>
                        </div>
                        <div className="chart-area">
                            <div>그래프</div>
                            <ul>
                                <li>적정</li>
                                <li>부족</li>
                                <li>없음</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="family-info">
                    <div className="table-title">
                        <p className="title">고객 보유계약 현황</p>
                        <p className="date"><i/>관리계약</p>
                    </div>
                    <table className="skyblue-table insu-list-table">
                        <colgroup>
                            <col style={{ width:"4%" }}/>
                            <col style={{ width:"9%" }}/>
                            <col />
                            <col style={{ width:"8%" }}/>
                            <col style={{ width:"5%" }}/>
                            <col style={{ width:"5%" }}/>
                            <col style={{ width:"7%" }}/>
                            <col style={{ width:"5%" }}/>
                            <col style={{ width:"6%" }}/>
                            <col style={{ width:"7%" }}/>
                            <col style={{ width:"6%" }}/>
                            <col style={{ width:"8%" }}/>
                            <col style={{ width:"8%" }}/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th></th>
                                <th>보험사</th>
                                <th>상품명</th>
                                <th>보험료</th>
                                <th>계약자</th>
                                <th>피보험자</th>
                                <th>계약일</th>
                                <th>납입주기</th>
                                <th>납입기간</th>
                                <th>보장종료일</th>
                                <th>납입회차</th>
                                <th>기납입보험료</th>
                                <th>잔여보험료</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>롯데손해보험</td>
                                <td>무배당 let:play 자녀보험(도담도담)(무해지형)</td>
                                <td>117,711원</td>
                                <td>김*애</td>
                                <td>정혜련</td>
                                <td>2021-08-20</td>
                                <td>월납</td>
                                <td>10년</td>
                                <td>2091-08-20</td>
                                <td>3 / 120</td>
                                <td>35만원</td>
                                <td>1,377만원</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>롯데손해보험</td>
                                <td>무배당 let:play 자녀보험(도담도담)(무해지형)</td>
                                <td>117,711원</td>
                                <td>김*애</td>
                                <td>정혜련</td>
                                <td>2021-08-20</td>
                                <td>월납</td>
                                <td>10년</td>
                                <td>2091-08-20</td>
                                <td>3 / 120</td>
                                <td>35만원</td>
                                <td>1,377만원</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </ReportContainer>
    );
};

export default CustomerInsu;