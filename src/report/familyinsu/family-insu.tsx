import React from "react";
import ReportContainer from "../report-container";

const FamilyInsu = () => {
    return (
        <ReportContainer>
            <h1 className="title">보장성보험 가족가입현황</h1>
            <p className="explain">남한희님 가족이 보유하고 있는 보장성 보험의 월 납입보험료는 총 <span className="stress">16만원</span>입니다.</p>
            <div className="family-insu">
                <div className="content-area">
                    <div className="content-box">
                        <div className="top">
                            <p>
                                <span>월 납입보험료 <strong>16</strong> 만원</span>
                            </p>
                            <p>
                                <span>보험계약 <strong>2</strong> 건</span>
                            </p>
                        </div>
                        <div className="bottom">
                            <img className="cus-img" src="/img/report/family2.png" alt="고객이미지"/>
                            <span>남한희(고객)</span>
                            <span><strong>16</strong> 만원 (2건)</span>
                        </div>
                    </div>
                    <div className="content-box chart-box">
                        <div className="chart-desc">
                            <i><img src="/img/report/icon-nothing.png" alt="수준 이미지"/></i>
                            <p className="chart-result">
                                월소득대비<br/>
                                <span>보험료 수준이 부족</span>합니다.  
                            </p>
                            <p className="chart-info">
                                <strong>월소득 600만원 / 30대 기준</strong>
                                <span>
                                    보장성 월보험료가 월소득의<br/>
                                    5% 초과일때는 과도, 3% 미만일때는<br/>
                                    부족하다고 할 수 있습니다.
                                </span>
                            </p>
                        </div>
                        <div className="chart-area">
                            dfdfdfdf
                        </div>
                    </div>
                </div>
                <div className="family-info">
                    <div className="table-title">
                        <p className="title">가족 기본정보</p>
                        <p className="date">기준일 : 2021-11-15</p>
                    </div>
                    <table className="skyblue-table">
                        <colgroup>
                            <col style={{ width:"8%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                            <col style={{ width:"11%" }}/>
                        </colgroup>
                        <thead>
                            <tr>
                                <th>구분</th>
                                <th>이름</th>
                                <th>생년월일</th>
                                <th>나이</th>
                                <th>보험나이</th>
                                <th>보험상령일</th>
                                <th>성별</th>
                                <th>월 보험료</th>
                                <th>특이사항</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>고객</td>
                                <td>남한희</td>
                                <td>1991-09-17</td>
                                <td>31세</td>
                                <td>30세</td>
                                <td>2022-03-17</td>
                                <td>여</td>
                                <td>16만원</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>고객</td>
                                <td>남한희</td>
                                <td>1991-09-17</td>
                                <td>31세</td>
                                <td>30세</td>
                                <td>2022-03-17</td>
                                <td>여</td>
                                <td>16만원</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>고객</td>
                                <td>남한희</td>
                                <td>1991-09-17</td>
                                <td>31세</td>
                                <td>30세</td>
                                <td>2022-03-17</td>
                                <td>여</td>
                                <td>16만원</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </ReportContainer>
    );
};

export default FamilyInsu;