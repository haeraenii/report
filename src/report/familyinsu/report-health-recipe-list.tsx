import React from 'react';

// type listBoxTitleProp = {
//     [key:string] : recipeProp;
// };

export type recipeListBox = {
    [key:string] : string[];
    
};

export type RecipeListProp = {
    mainTitle: string;
    subTitle: string;
    listBoxTitle: recipeListBox;
};

type recipeProp = {
    className: string;
    image: string;
    title: string;
    list: string[];
};

const RecipeListDetail = ({className, image, title, list}:recipeProp) => {

    console.log(list);

    return (
        <>
            <div className="recipe-box">
                <p className={`recipe-title ${className}`}><i className="title-icon"><img src={`/img/report/health/icon-${image}@2x.png`} alt=""/></i>{title}</p>
                <ul className="recipe-list">
                    {list.map((li)=>(<li>{li}</li>))}
                </ul>
            </div>
        </> 
    );
};

export type recipeListCon = {
    [key:string] : RecipeListProp;
};

export type listProp = {
    recipe: recipeListCon;
};

const ReportHealthRecipeList = ({recipe}:listProp) => {
    return (
        <>  
            <h2>암 특약 레시피<span>암 관련 치료비를 위한 유용한 특약</span></h2>
            <div className="recipe-area">
                <RecipeListDetail className="blue" image="doctor" title="암진단비" list={recipe.cancer.listBoxTitle.diagnose}/>
                <RecipeListDetail className="green" image="surgery" title="암 수술비 · 암 입원비" list={recipe.cancer.listBoxTitle.operation}/>
                <RecipeListDetail className="navy" image="medicine" title="항암치료비 · 기타" list={recipe.cancer.listBoxTitle.etc}/>
            </div>
        </>
    );
};

export default ReportHealthRecipeList;