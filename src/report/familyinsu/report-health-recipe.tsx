import React from 'react';
import ReportContainer from '../report-container';
import ReportHealthRecipeList, { recipeListCon } from './report-health-recipe-list';

const cancerDiagnose:string[] = ["고액암 진단","일반암(중대한암) 진단","소액암 진단","재진단시 암진단비","특정부위별 추가 암진단비"];
const cancerOperation:string[] = ["암 수술비","암 입원비","요양병원 입원비","암 통원치료"];
const cancerEtc:string[] = ["표적항암치료비","암 진단 생활비","말기암 호스피스 치료비","폴립 진단/수술비","바늘생검 진단비"];
const brainheartDiagnose:string[] = ["고액암 진단","일반암(중대한암) 진단","소액암 진단","재진단시 암진단비","특정부위별 추가 암진단비"];
const brainheartOperation:string[] = ["암 수술비","암 입원비","요양병원 입원비","암 통원치료"];
const brainheartEtc:string[] = ["표적항암치료비","암 진단 생활비","말기암 호스피스 치료비","폴립 진단/수술비","바늘생검 진단비"];
const dementiaDiagnose:string[] = ["고액암 진단","일반암(중대한암) 진단","소액암 진단","재진단시 암진단비","특정부위별 추가 암진단비"];
const dementiaOperation:string[] = ["암 수술비","암 입원비","요양병원 입원비","암 통원치료"];
const dementiaEtc:string[] = ["표적항암치료비","암 진단 생활비","말기암 호스피스 치료비","폴립 진단/수술비","바늘생검 진단비"];

const RecipeList:recipeListCon = {
    "cancer": {
        mainTitle:"암",
        subTitle:"암",
        listBoxTitle:{
            "diagnose":cancerDiagnose,
            "operation":cancerOperation,
            "etc":cancerEtc
        }
    },
    "brainheart": {
        mainTitle:"뇌혈관질환과 심장질환",
        subTitle:"2대 질병",
        listBoxTitle:{
            "diagnose":brainheartDiagnose,
            "operation":brainheartOperation,
            "etc":brainheartEtc
        }
    },
    "dementia":{
        mainTitle:"치매",
        subTitle:"치매",
        listBoxTitle:{
            "diagnose":dementiaDiagnose,
            "operation":dementiaOperation,
            "etc":dementiaEtc
        }
    }
};


// const RecipeList:recipeListCon = {
//     "cancer": {
//         mainTitle:"암",
//         subTitle:"암",
//         listBoxTitle:{
//             "diagnose":
//             {
//                 classname:"blue",
//                 image:"doctor",
//                 title:"암진단비",
//                 list:cancerDiagnose
//             },
//             "operation":
//             {
//                 classname:"green",
//                 image:"surgery",
//                 title:"암 수술비 · 암 입원비",
//                 list:cancerOperation
//             },
//             "etc":
//             {
//                 classname:"navy",
//                 image:"medicine",
//                 title:"항암치료비 · 기타",
//                 list:cancerEtc
//             }
//         }
//     },
//     "brainheart": {
//         mainTitle:"뇌혈관질환과 심장질환",
//         subTitle:"2대 질병",
//         listBoxTitle:{
//             "diagnose":
//             {
//                 classname:"blue",
//                 image:"doctor",
//                 title:"2대 질병 진단비",
//                 list:brainheartDiagnose
//             },"operation":
//             {
//                 classname:"green",
//                 image:"surgery",
//                 title:"2대 질병 수술비 · 입원비",
//                 list:brainheartOperation
//             },"etc":
//             {
//                 classname:"navy",
//                 image:"medicine",
//                 title:"기타특약",
//                 list:brainheartEtc
//             }
//         }
//     },
//     "dementia":{
//         mainTitle:"치매",
//         subTitle:"치매",
//         listBoxTitle:{
//             "diagnose":{
//                 classname:"navy",
//                 image:"medicine",
//                 title:"기타특약",
//                 list:dementiaDiagnose
//             },
//             "operation":{
//                 classname:"navy",
//                 image:"medicine",
//                 title:"기타특약",
//                 list:dementiaOperation
//             },
//             "etc":{
//                 classname:"navy",
//                 image:"medicine",
//                 title:"기타특약",
//                 list:dementiaEtc
//             }
//         }
//     }
// };


const ReportHealthRecipe = () => {
    return (
        <ReportContainer>
            <h1 className="title">건강분석 기반 보장분석 &gt; <span className="cus-name">남한희</span>님</h1>
            <p className="explain">AI분석 결과 주의와 관심이 필요한 주요 질환에 대한 구체적인 치료방법과 치료비용 정보입니다.</p>
            <div className="health-analysis-recipe">
                <ReportHealthRecipeList recipe={RecipeList}/>
            </div>
        </ReportContainer>
    );
};

export default ReportHealthRecipe;