import React from "react";

type ReportProp = {
    children: React.ReactNode;
};

const ReportContainer = ({children}:ReportProp) => {
    return (
        <div className="report-container">
            <div className="report-main">
                {children}
            </div>
            <p className="side-title">
                보장설계
            </p>
            <div className="report-footer">
                <p className="noti">
                    본 분석자료는 고객님의 이해를 돕기 위한 목적으로 제시한 것으로, 각 보험계약별 보장내용 등에 있어서 실제와 다를 수 있으므로 정확한 내용은 상품 약관 및 보험증권을 참조하시기 바랍니다.
                </p>
                <p className="name">작성자 : 정혜련 <span>1/10</span></p>
            </div>
        </div>
    );  
};

export default ReportContainer;