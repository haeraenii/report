import React from "react";
import { Navigation, Scrollbar } from "swiper";
import {Swiper, SwiperSlide} from 'swiper/react/swiper-react.js';
import CustomerInsu from "./report/familyinsu/customer-insu";
import FamilyInsu from "./report/familyinsu/family-insu";
import ReportHealthRecipe from "./report/familyinsu/report-health-recipe";

const RootLayout = () => {

    return (
        <div className="result-report">
            <div className="report-wrap">
                <Swiper
                    modules={[Navigation, Scrollbar]}
                    spaceBetween={0}
                    slidesPerView={1}
                    loop={false}
                    navigation={{ 
                        prevEl: '.prev-btn',
                        nextEl: '.next-btn'
                    }}
                >
                    <SwiperSlide><ReportHealthRecipe/></SwiperSlide>
                    <SwiperSlide><FamilyInsu/></SwiperSlide>
                    <SwiperSlide><CustomerInsu/></SwiperSlide>
                    <SwiperSlide><CustomerInsu/></SwiperSlide>
                </Swiper>
                <button className="prev-btn"/>
                <button className="next-btn"/>
            </div>
        </div>      
    );
};

export default RootLayout;