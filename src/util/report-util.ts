import SwiperCore from "swiper";

export function stDateFullDate (stDate: string, sep: string = '.') : string {
    if (!stDate || stDate.length !== 8)
        return stDate;

    return `${stDate.substring(0, 4)}${sep}${stDate.substring(4, 6)}${sep}${stDate.substring(6, 8)}`;
}

export function stDateMMdd (stDate: string, sep: string = '/') : string {
    if (!stDate || stDate.length !== 8)
        return stDate;

    return `${stDate.substring(4, 6)}${sep}${stDate.substring(6, 8)}`;
}

export function moveToSlide (reportId: string, controlledSwiper?: SwiperCore) {
    if (!controlledSwiper)
        return;

    let reportIdx = -1;

    for (let idx = 0; idx < controlledSwiper.slides.length; idx++) {
        if (controlledSwiper.slides[idx].getAttribute("data-id") === reportId) {
            reportIdx = idx;
            break;
        }
    }

    if (reportIdx >= 0)
        controlledSwiper?.slideTo(reportIdx);
}

export function twoNumber(val: number): string {
    if (val < 10) return `0${val}`;

    return val.toString()
}